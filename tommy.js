function HorizontalSmoothScroll(target, speed, smooth, autobreak) {
    var moving = false;
    var autoBreaking = false;
    var pos = parseInt(getTranslate3d(target)[0])
    target.addEventListener('wheel', onScroll);

    //For debugging
    var testUpdate = 0;

    function onScroll(e) {
        e.preventDefault();

        var trans3d = getTranslate3d(this);
        var transX = parseInt(trans3d[0]);
        var _newX = transX + e.deltaY;

        console.log("e.deltaY:", e.deltaY);

        // var delta = normalizeWheelDelta(e);
        // console.log("normalizeWheelDelta:", delta);
        var delta = -e.deltaY / 40; //先只取用Ｙ值

        pos += delta * speed;
        console.log("pos:", pos);
        if (delta > 0 && pos >= 0) {
            pos = 0;
            autoBreaking = true;
        } else if (delta < 0 && pos <= -this.offsetWidth + window.outerWidth) {
            pos = -(this.offsetWidth - window.outerWidth);
            autoBreaking = true;
        }


        if (!moving) {
            update();
        }

    }

    function normalizeWheelDelta(e) {
        console.log(e.wheelDelta)
        if (e.detail) {
            if (e.wheelDelta)
                return e.wheelDelta / e.detail / 40 * (e.detail > 0 ? 1 : -1) // Opera
            else
                return -e.detail / 3 // Firefox
        } else {
            // console.log(e.wheelDelta)
            return e.wheelDelta / 120 // IE,Safari,Chrome
        }
    }

    function update() {
        moving = true;
        var tarPosX = parseInt(getTranslate3d(target)[0]);
        var delta = (pos - tarPosX) / smooth;
        console.log("delta:", delta);
        // if(autobreak){

        // } else {
        //     delta = (pos - tarPosX) / smooth;
        // }

        tarPosX += delta
        target.style.transform = "translate3d(" + tarPosX + "px, 0, 0)";
        console.log("tarPosX:", tarPosX);
        // console.log("Math.abs(delta) =",Math.abs(delta));
        if (Math.abs(delta) > 1) {
            // console.log("Math.abs(delta) > 0.2? Math.abs(delta) =",Math.abs(delta));
            // console.log("requestFrame(update);");
            requestFrame(update);
        } else {
            moving = false;
            autobreak = false;
        }
    }

    var requestFrame = function () { // requestAnimationFrame cross browser
        return (
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (func) {
                window.setTimeout(func, 1000 / 60);
            }
        );
    }()
}

